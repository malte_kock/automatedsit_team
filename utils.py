from typ_code import typ1 as type1, typ3 as type3, typ4 as type4, typ5 as type5, typ6 as type6, typ7 as type7, typ8 as type8, typ9 as type9, typ10 as type10, typ11 as type11, typ12 as type12, type_waypoints_text

import xml.etree.ElementTree as ET
import XML_utils as XML
import verify_utils as vu

# Enter lines that have been analysed but not fixed through code.
# All lines get turned into user_actions but don't show up as a bug report.
#known_verify_bugs = [991, 987, 975, 283, 336, 384, 382, 400]

def analyse(row):
    global status
    instruction = row[0]
    if instruction.startswith('Note') or instruction.startswith('NOTE'):  # check if row contains note or instruction
        return "note"

    elif type1.analyse(instruction):
        return "type1"

    elif type3.analyse(instruction):
        return "type3"

    elif type4.analyse(instruction):
        return "type4"

    elif type5.analyse(instruction):
        return "type5"

    elif type6.analyse(instruction):
        return "type6"

    elif type7.analyse(instruction):
        return "type7"

    elif type8.analyse(instruction):
        return "type8"

    elif type9.analyse(instruction):
        return "type9"

    elif type10.analyse(instruction):
        return "type10"

    elif type11.analyse(instruction):
        status = 'way_start'
        return "type11"

    elif type12.analyse(instruction):
        return "type12"

    elif type_waypoints_text.analyse(instruction) and (status == 'way_start' or status == 'way'):
        status = 'way'
        return "type_waypoints_text"

    else:
        status = 'no_way'
        return "no_type"


def extract(row, type):
    instruction = row[0]
    if type == "note":
        return []

    elif type == "no_type":
        return []

    elif type == "type1":
        info = type1.extract(instruction)
        return info

    elif type == "type3":
        info = type3.extract(instruction)
        return info

    elif type == "type4":
        info = type4.extract(instruction)
        return info

    elif type == "type5":
        info = type5.extract(instruction)
        return info

    elif type == "type6":
        info = type6.extract(instruction)
        return info

    elif type == "type7":
        info = type7.extract(instruction)
        return info

    elif type == "type8":
        info = type8.extract(instruction)
        return info

    elif type == "type9":
        info = type9.extract(instruction)
        return info

    elif type == "type10":
        info = type10.extract(instruction)
        return info

    elif type == "type11":
        info = type11.extract(instruction)
        return info

    elif type == "type12":
        info = type12.extract(instruction)
        return info

    elif type == "type_waypoints_text":
        info = type_waypoints_text.extract(instruction)
        return info

    else:
        raise Exception("type not implemented")


def createXMLnode(type, line, row, info):
    # Create node that will be returned
    node = ET.Element("instruction")

    # Append debugging information to the node
    debug_info_element = ET.Comment(
        "Debugging:" + "\nInstruction of type: " + type + "\nDocument line: " + str(line))
    node.append(debug_info_element)

    if type == "note":
        note_element = ET.Comment(
            "### Note: ###" + row[0])
        node.append(note_element)

    elif type == "no_type":
        XML.xml_no_type(node, row[0])

    elif type == "type1":
        voice_current = info['Voice']
        tone_current = info['Tone']
        global voice_last
        global tone_last
        if voice_current == "repeat last voice" or tone_current == "repeat last tone":
            XML.xml_listen_voice_tone(node, voice_last, tone_last)
        else:
            voice_last = info['Voice']
            tone_last = info['Tone']
            XML.xml_listen_voice_tone(node, voice_current, tone_current)


    elif type == "type3":

        if info['Time'] != 'None':
            XML.xml_wait(node, info['Time'])
        for j in range(len(info['Device'])):
            XML.xml_display_read(node, info['Device'][j], info['Symbol'], info['Text'], info['Col'], info['Bcol'], info['Verify'], info['Box'], info['State'], info['Notification_type'])

    elif type == "type4":
        XML.xml_gui(node, info['Device'], info['Interface'], info['Control_box'], info['State'])

    elif type == "type5":
        XML.typ5(node, info['Action'], info['Device'], info['Switch'])

    elif type == "type6":
        for p in range(len(info['Device'])):
            XML.xml_type_press(node, info['Device'][p], info['Action'], info['Text'], info['Button'], info['Select'], info['Num_type_press'])

    elif type == "type7":
        XML.xml_lamp(node, info['Device'], info['Lamp'], info['Location'], info['State'])

    elif type == "type8":
        XML.xml_wait(node, info['Time'])

    elif type == "type9":
        for k in range(len(info['Breaker'])):
            XML.xml_breaker(node, info['Action'], info['Breaker'][k], info['Location'])

    elif type == "type10":
        XML.xml_cdu_verify_read(node, info['Device'], info['Text'], info['Location'], info['File'], info['Title'], info['Symbol'])

    elif type == "type11":
        global node_waypoints
        node_waypoints = node
        XML.xml_waypoints(node_waypoints, info['Device'], info['Location'])
        #node = node_waypoints

    elif type == "type_waypoints_text":
        try:
            verify_actions(node_waypoints, type)
            for ele in node_waypoints[1]:
                if ele.tag == "Text":
                    XML.xml_waypoints_text(ele, info['Waypoint'])
            node.remove(debug_info_element)
        except vu.VerifyException:
            XML.xml_no_type(node, row[0])

    elif type == "type12":
        for q in range(len(info['Device'])):
            XML.xml_cdu_verify_display(node, info['Device'][q], info['Page'], info['Symbol'], info['Text'], info['Location'], info['Verify'])

    else:
        raise Exception("Type not implemented.")

    return node

def verify_actions(node, line_type):
    for element in node:
        if line_type =="note":  # Notes don't have to get verified.
            raise vu.NoteException

        elif element.tag == ET.Comment:  # Debugging Comments don't have to get verified
            continue

        elif element.tag == "User_action":  # User_actions always get verified the same way, as implemented in main.py
            raise vu.UserActionException

        elif element.tag == 'Type':
            vu.verify_type(element)
            continue

        elif element.tag == 'Press':
            vu.verify_press(element)
            continue

        elif element.tag == 'Pull' or element.tag == 'Push':
            vu.verify_pull_push(element)
            continue

        elif element.tag == 'Listen':
            vu.verify_listen_voice_tone(element)
            continue

        elif element.tag == 'Read':
            if element.attrib == {'type': 'Waypoints'}:
                vu.verify_waypoints(element)
            else:
                vu.verify_read(element)
            continue

        elif element.tag == 'Check':
            vu.verify_check(element)
            continue

        elif element.tag == 'Wait':
            vu.verify_wait(element)

        elif element.tag == 'Set':
            vu.verify_set(element)

            continue

        else:
            raise vu.NotImplementedException("The node contains actions that have no verification function. (not implemented?)")

def createNodeReport(node, info, row):
    for action in node:
        if action.tag == ET.Comment:
            print(str(action.text))
            print("Extracted information: " + str(info))
            print("Original document field is: " + row)  # Uncommented since this information si already contained in Line content
