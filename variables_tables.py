###############################################################################################
help_table = []
###############################################################################################

type_devices = ['CSE Computer', 'CSE scripting window', 'CDU #1', 'CDU #2', 'V/UHF BCP']

type_master = {
    "Device": type_devices,
}

###############################################################################################

press_devices = ['CSE Computer', 'Console Switch Panel', 'center console', 'cockpit console', 'CDU #1', 'CDU #2',
                 'V/UHF BCP', 'V/UHF CP']
press_buttons = ['R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6', '↓', '↑',
                 'CLR', '"CLR"', 'Enter', 'EXEC', 'IDX', 'DIR', 'FPLN', 'LEGS', 'MSN', 'PTT',
                 '<V1>', '<↓>', '<COM>', '<CLR>', 'COM', '<DOMAIN>', '<MAN>',
                 '<LS1L>', '<LS2L>', '<LS3L>', '<LS4L>', '<LS5L>', '<LS6L>',
                 '<LS1R>', '<LS2R>', '<LS3R>', '<LS4R>', '<LS5R>', '<LS6R>']
press_switches = ['Copilot M/C RESET GRIP PNL Switch', 'Pitch Down Transition Switch', 'FIRE ACK Switch']
press_select = ['RW13C', 'TACRT01', 'CHATY2', 'RZS', 'TACT01', '"TR+G"', '"COAST"', '"SHIP"', '"40.5"', '"121.5"', '"243"',
                '"ON"', '"OFF"', '"1"', '"TR"', '"GD"', '"SCAN"', '"AUTO"', 'MEDIUM', '"PTEC"', '"ATEC"', 'LOW', 'HIGH',
                '"SELFINIT"', '"GPS"', '"DATE"', '"TIME"', '"0"']

press_master = {
    "Device": press_devices,
    "Button": press_buttons,
    "Switch": press_switches,
    "Select": press_select
}

###############################################################################################

pull_push_device = ['SIL']
pull_push_breaker = ['1DCU 26 VAC', '2DCU 26 VAC', '1IPC', '2IPC']
pull_push_location = ['bottom row of the SIL Rack', 'SIL Rack']

pull_push_master = {
    "Device": pull_push_device,
    "Breaker": pull_push_breaker,
    "Location": pull_push_location,
}

###############################################################################################

listen_voice_tone_devices = ['Warning Tone Generator']
listen_voice_tone = ['None', '"FIRE ENGINE ONE FIRE"', '"FIRE ENGINE TWO FIRE"', '"HANDS ON HANDS ON"',
                     '"FIRE APP FIRE"', '"LANDING GEAR"', '"IFF MODE FOUR"', 'Low Frequency Tone', 'Chime Tone',
                     'dedicated Tone', 'Modulated Tone']

listen_voice_tone_master = {
    "Device": listen_voice_tone_devices,
    "Tone_Voice": listen_voice_tone
}

###############################################################################################

read_devices = ['MFD#1', 'MFD#2', 'MFD#3', 'MFD#4', 'MFD#5', 'CDU #1', 'CDU #2']
read_box = ['LDG status box']
read_state = ['Drawn', 'BLANK', 'Blank', 'not displayed', 'displayed']
read_text_MFD = ['"\\\\"', '"ACFAN"', '"AFCS"', '"AFLOW"', '"APP"', '"AUDIO"', '"BATLOW"',
 '"BATLOW""', '"BATOFF"', '"BIM"', '"BLFLD"', '"CHIP"', '"DCU1"', '"DCU2"', '"DISCH"',
 '"DN"', '"EAPS"', '"ENG 1"', '"ENG 2"', '"ENGPHI"', '"ENGPLO"', '"EXTPWR"', '"FILTER"',
 '"FIRE"', '"FIREAP"', '"GEN"', '"HOOK"', '"ICING"', '"IFF"', '"IMB"', '"ISOVLV"',
 '"LAN 1/2"', '"LOW"', '"MFIRE"', '"PIT 1"', '"PIT 2"', '"PKBRK"',
 '"PRESS"', '"PUMP"', '"RAMP"', '"RECT"', '"RECT3"', '"RTRBKP"', '"RTRBRK"', '"START"', '"SVOUT"', '"TEMP"', '"TRSV2"']
read_text_CDU = ['ACT', 'KJFK', 'KPIT', '"RW13C"', 'KORD', 'KLGA', 'waypoint AIO', 'DISCONTINUITY', 'DSM waypoint',
                 'V27 airway VIA', 'OAK K2 VORTAC', 'navaid OAK', '"INSERT CIR BEFORE?"', '"INSERT IMP BEFORE?"',
                 '"INSERT LDR BEFORE?"', '"√\xa0IMP PATTERN"', '"WPT01 STORED"', 'user-defined waypoint WPT01',
                 'LL005', 'waypoint RW19L',
                 '"OFF "', '"ANY frequency"', '"DOMAIN "', '"0"', '"1"', '"LOW"', '"TR+G"', '"HIGH"', '"GPS"',
                 '"Missing"', '"MISSING"', '"UNAVAILABLE"', '"[][][][]"', '"TRAINING"', '"NO FAULTS"', '"INIT"',
                 '"AVAIL"', '"NOFAULTS"']

read_symbol1_MFD = ['', 'ICE', 'MISC', 'AC2', 'UTIL', 'ENG 1', 'ENG 2', '"BLFLD"', 'AVIONICS', 'AXMSN', 'MXMSN', 'IXMSN',
                'TXMSN', 'HYD1', 'HYD2', 'BAT 1', 'BAT 2', 'NXMSN 1', 'FUEL 1', 'NXMSN 2', 'AC 2', 'BAY L', 'DC 1',
                'DC 2', 'AC 1', 'BAY AFT', 'BAY R', 'FUEL 2']
read_symbol1_CDU = ['R3', '"V1√"', '"V2√"', '"OFF"', '"DOMAIN"', '"MAIN SQL"', '"MODE"', '"TX POWER"', '"GUARD SQL"',
                    '"AIRPORT"', '"RCVR"', '"XMIT"', '"WOD"', '" SELECT"', '"TOD"', '"GUARD VOL"', '"CBIT"', '"IBIT"',
                    ]
read_symbol2 = ['Lower Left', 'Upper Center', 'Lower Right', 'All Three LDG', 'Nose and Right LDG', 'all three LDGs', 'Left LDG']

read_location = ['Title Line', 'modified flight plan after DSM', 'active flight plan before AIO', 'active flight plan',
                  'scratchpad', 'modified flight plan',
                 'data line 1', 'data line 2', 'data line 3', 'data line 4', 'data line 5', 'data line 6',
                 'label line 1', 'label line 2', 'label line 3', 'label line 4', 'label line 5',
                 'title line 1']
read_file = ['modified flight plan']
read_title = ['"MOD"']

read_master = {
    "Device": read_devices,
    "Symbol": read_symbol1_MFD + read_symbol1_CDU + read_symbol2,  # read_symbol2 are problematic
    "Text": read_text_MFD + read_text_CDU,
    "Box": read_box,
    "Location": read_location,
    "State": read_state,
    "File": read_file,
    "Title": read_title
}

###############################################################################################

waypoints_device = ['CDU #1', 'CDU #2']
waypoints_location = ['HISTORY', 'ACT LEGS page', 'LEGS page', 'LEGS Page', 'active flight plan']

waypoints_master = {
    "Device": waypoints_device,
    "Location": waypoints_location
}
###############################################################################################

check_devices = ["CWA Panel"]
check_lamp = ['"CHIP"', '"UTILP"', 'Family "AVCS"', '"BAY T"', '"CHIP"', '"LDGUP"', '"ECHIP"', 'Family "ENG"',
              'Family "HYD"', 'Family "XMSN"', '"CHIPN"', 'Family "ICE"', 'Family "ELEC"', 'Family "FUEL"', '"MFIRE"',
              ]
check_state = ['On', 'Off', 'Flashes once']
check_location = ['Left-hand (1)', 'Right-hand (2)', 'Left (1)', 'Right (2)']

check_master = {
    "Device": check_devices,
    "Lamp": check_lamp,
    "State": check_state,
    "Location": check_location
}

###############################################################################################

set_device = ['CSE', 'Console Switch Panel']
set_interface = ['"DCU INPUTS 1" Main GUI', '"DCU_INPUTS_1" Main GUI', '"AC_707_Model" GUI', '"AC_707_MODEL" GUI',
                 '"ADC 1" GUI', '"ADC 2" GUI',
                 '"radar_altimeter_1_gui"', '"radar_altimeter_1_gui" GUI', '"ew_1_gui"', '"AHRS_1" GUI', '"AHRS_2" GUI']
set_control_box = [
    '"DCMB Failed"',
    'Accessory Xmsn Chip Detected 1',
    'Accessory Xmsn Chip Detected 2',
    'Accessory Xmsn Oil Press Lo',
    'Accessory Xmsn Oil Temp Hi',
    'Altitude Bias',
    'APU Eng Fire',
    'APU Eng In Operation',
    'Avionics Bay Left Airflow Fail',
    'Avionics Bay Rear Ac Fan Fail',
    'Avionics Bay Rear Airflow Fail',
    'Avionics Bay Right Airflow Fail',
    'Baro Altitude',
    'Batt 1 Discharging',
    'Batt 1 Switch Off',
    'Batt 1 Temp Hi',
    'Batt 1 Voltage Lo',
    'Batt 2 Discharging',
    'Batt 2 Switch Off',
    'Batt 2 Temp Hi',
    'Batt 2 Voltage Lo',
    'Bim Failure Detected',
    'Cargo Hook Open',
    'Cargo Ramp Door Open',
    'Computed Airspeed Override',
    'Dcmb1 Batt Status',
    'Dcmb2 Tru Status',
    'Eng 1 Anti Ice On',
    'ENG 1 Anti Ice System Fail',
    'Eng 1 Anti Ice System Fail',
    'Eng 1 Chip Detected',
    'Eng 1 Eaps Bypass Doors Closed',
    'Eng 1 Fire',
    'Eng 1 Oil Press Hi',
    'Eng 1 Oil Press Lo',
    'Eng 1 Particle Separator Failed',
    'Eng 1 Starter Circuit Energized',
    'Eng 2 Anti Ice On',
    'ENG 2 Anti Ice System Fail',
    'Eng 2 Anti Ice System Fail',
    'Eng 2 Chip Detected',
    'Eng 2 Eaps Bypass Doors Closed',
    'Eng 2 Fire',
    'Eng 2 Oil Press Hi',
    'Eng 2 Oil Press Lo',
    'Eng 2 Particle Separator Failed',
    'Eng 2 Starter Circuit Energized',
    'Eng Fire 1',
    'Eng Fire 2',
    'Ext Pwr Connected',
    'Fuel 1 Lo',
    'Fuel 2 Lo',
    'Fuel Filter 1 Clogged',
    'Fuel Filter 2 Clogged',
    'Fuel Pump 1 Failed',
    'Fuel Pump 2 Failed',
    'Fuel Qty Lt Main Tank',
    'Fuel Qty Rt Main Tank',
    'Fuel Quantity Lt Main Tank',
    'Fuel Quantity Rt Main Tank',
    'Gear Lt Dn',
    'Gear Lt Up',
    'Gear Nose Dn',
    'Gear Nose Up',
    'Gear Rt Dn',
    'Gear RT Dn',
    'Gear RT Up',
    'Gear Rt Up',
    'Gen 1 Failed',
    'Gen 2 Failed',
    'Hydr Oil Press 1 Lo',
    'Hydr Oil Press 2 Lo',
    'Hydr System 1 Servo Fail',
    'Hydr System 2 Servo Fail',
    'Hydr Tail Rotor Servo Fail',
    'Hydr Utility Oil Temp High',
    'Hydr Utility System Isolation Valve Open',
    'Ice Conditions Detected',
    'IFF Mode 4 Relay',
    'Indicated Airspeed',
    'Int Xmsn Chip Detected',
    'Int Xmsn Oil Temp Hi',
    'Main Xmsn Chip Detected 1',
    'Main Xmsn Chip Detected 2',
    'Main Xmsn Chip Detected 3',
    'Main Xmsn Chip Detected 4',
    'Main Xmsn Oil Press Lo',
    'Main Xmsn Oil Temp Hi',
    'Nose Gear Up',
    'Nose Xmsn 1 Chip Detected',
    'Nose Xmsn 1 Oil Press Lo',
    'Nose Xmsn 1 Oil Temp Hi',
    'Nose Xmsn 2 Chip Detected',
    'Nose Xmsn 2 Oil Press Lo',
    'Nose Xmsn 2 Oil Temp Hi',
    'Parking Brake Act',
    'Pitch',
    'Pitch Bias',
    'Pitot 1 Pwred',
    'Pitot 2 Pwred',
    'Rad Alt Ssm',
    'Rad Alt Ssm (164)',
    'Radar Altitude Safety Degraded',
    'Rotor Blades Folded',
    'Rotor Brake Act',
    'Rotor Brake Pressurized',
    'Sdm 1 Misfire Cnt A',
    'Tail Xmsn Chip Detected',
    'Tail Xmsn Oil Temp H',
    'Tail Xmsn Oil Temp Hi',
    'Tru 1 Failed',
    'Tru 2 Failed',
    'Tru 3 Failed',
    'Utility Hydr Oil Press']

set_state = ['Active', 'ACTIVE', 'Inactive', 'INACTIVE', 'Normal', '"DCMB Failed"', '"DCMB Ok"', '0', '1']

set_master = {
    "Device": set_device,
    "Interface": set_interface,
    "Control_box": set_control_box,
    "State": set_state
}

###############################################################################################

units = ['kgf/cm 2', 'feet', 'ft', 'ft.', 'knots', 'lbs', 'Degrees', 'degrees', 'kts']
