import xml.etree.ElementTree as ET
# Subroutine to generate xml-file

def xml_no_type(root, instruction):
    element = ET.SubElement(root, 'User_action')
    element.text = instruction

def xml_listen_voice_tone(root, voice, tone):
    act = ET.SubElement(root, 'Listen')
    dev = ET.SubElement(act, 'Device')
    dev.text = 'Warning Tone Generator'
    voiceElem = ET.SubElement(act, 'Voice')
    voiceElem.text = voice
    toneElem = ET.SubElement(act, 'Tone')
    toneElem.text = tone

def xml_display_read(root, device, symbol, text, col, bcol, verify, box, state, notification_type):

    act = ET.SubElement(root, 'Read')
    dev = ET.SubElement(act, 'Device')
    dev.text = device

    if box != 'None':
        bo = ET.SubElement(act, 'Box')
        bo.text = box
        if type(state) == list:
            state1 = state[0]
            #state2 = state[1]
        else:
            state1 = state
            #state2 = 'None'
        sta = ET.SubElement(act, 'State')
        sta.text = state1

        if symbol != 'None':
            act2 = ET.SubElement(root, 'Read')
            dev = ET.SubElement(act2, 'Device')
            dev.text = device
            sym = ET.SubElement(act2, 'Symbol')
            sym.text = symbol
            if text != 'None':
                te = ET.SubElement(act2, 'Text', color=col, backgroundcolor=bcol)
                te.text = text
            else:
                sta = ET.SubElement(act2, 'State')
                sta.text = state[1]
    else:
        sym = ET.SubElement(act, 'Symbol')
        sym.text = symbol
        noti_typ = ET.SubElement(act, 'Notification')
        noti_typ.text = notification_type
        tex = ET.SubElement(act, 'Text', color=col, backgroundcolor=bcol)
        tex.text = text

        ver = ET.SubElement(act, 'State')
        ver.text = verify

    #ver = ET.SubElement(act, 'Verify')
    #ver.text = verify

def xml_gui(root, device, interface, control_box, state):
    act = ET.SubElement(root, 'Set')
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    interf = ET.SubElement(act, 'Interface')
    interf.text = interface
    schaltfl = ET.SubElement(act, 'Control_box')
    schaltfl.text = control_box
    stat = ET.SubElement(act, 'State')
    stat.text = state


def typ5(root, action, device, switch):
    act = ET.SubElement(root, action)
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    swi = ET.SubElement(act, 'Switch')
    swi.text = switch


def xml_type_press(root, device, action, text, button, select, num_type_press):
    if len(action) == 2:                            # action = ["type", "press"]
        if num_type_press > 0:                       # press first
            act = ET.SubElement(root, action[1])
            dev = ET.SubElement(act, 'Device')
            dev.text = device
            bot = ET.SubElement(act, 'Button')
            bot.text = button
            act = ET.SubElement(root, action[0])
            dev = ET.SubElement(act, 'Device')
            dev.text = device
            tex = ET.SubElement(act, 'Text')
            tex.text = text
        else:                                        # Type first
            act = ET.SubElement(root, action[0])
            dev = ET.SubElement(act, 'Device')
            dev.text = device
            tex = ET.SubElement(act, 'Text')
            tex.text = text
            act = ET.SubElement(root, action[1])
            dev = ET.SubElement(act, 'Device')
            dev.text = device
            bot = ET.SubElement(act, 'Button')
            bot.text = button

    elif button != 'None':                          # only Press
        act = ET.SubElement(root, action[0])
        dev = ET.SubElement(act, 'Device')
        dev.text = device
        bot = ET.SubElement(act, 'Button')
        bot.text = button
        if select != "None":
            sel = ET.SubElement(act, 'Select')
            sel.text = select

    else:                                         # only Type
        act = ET.SubElement(root, action[0])
        dev = ET.SubElement(act, 'Device')
        dev.text = device
        tex = ET.SubElement(act, 'Text')
        tex.text = text
        #if position != "None":
            #weh = ET.SubElement(act, 'Position')
            #weh.text = position
        # Only type orders, have to be confirmed by pressing enter afterwards
        act2 = ET.SubElement(root, "Press")
        dev = ET.SubElement(act2, 'Device')
        dev.text = 'CSE Computer'
        bot = ET.SubElement(act2, 'Button')
        bot.text = 'Enter'


def xml_lamp(root, device, lamp, location, state):
    act = ET.SubElement(root, 'Check')
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    la = ET.SubElement(act, 'Lamp')
    la.text = lamp
    if location != 'None':
        loc = ET.SubElement(act, 'Location')
        loc.text = location
    sta = ET.SubElement(act, 'State')
    sta.text = state


def xml_wait(root, time):
    act = ET.SubElement(root, 'Wait')
    tim = ET.SubElement(act, 'Time')
    tim.text = time


def xml_breaker(root, action, breaker, location):
    act = ET.SubElement(root, action)
    dev = ET.SubElement(act, 'Device')
    dev.text = 'SIL'
    bre = ET.SubElement(act, 'Breaker')
    bre.text = breaker
    loc = ET.SubElement(act, 'Location')
    loc.text = location


def xml_cdu_verify_read(root, device, text, location, file, title, symbol):
    act = ET.SubElement(root, 'Read')
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    if file == "None":
        tex = ET.SubElement(act, "Text")
        tex.text = text
        if location != "None":
            loc = ET.SubElement(act, "Location")
            loc.text = location
    else:
        fi = ET.SubElement(act, "File")
        fi.text = file
        tit = ET.SubElement(act, "Title")
        tit.text = title
        if symbol != "None":
            sym = ET.SubElement(act, "Symbol")
            sym.text = symbol
            tex = ET.SubElement(act, "Text")
            tex.text = text


def xml_waypoints(root, device, location):
    act = ET.SubElement(root, 'Read', type='Waypoints')
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    loc = ET.SubElement(act, 'Location')
    loc.text = location
    tex = ET.SubElement(act, 'Text')


def xml_waypoints_text(root, waypoint):
    way = ET.SubElement(root, 'Waypoint')
    way.text = waypoint


def xml_cdu_verify_display(root, device, page, symbol, text, location, verify):
    act = ET.SubElement(root, 'Read')
    dev = ET.SubElement(act, 'Device')
    dev.text = device
    if page != "None":
        pa = ET.SubElement(act, "Page")
        pa.text = page
    if symbol != "None":
        sym = ET.SubElement(act, "Symbol")
        sym.text = symbol
        tex = ET.SubElement(act, "Text")
        tex.text = text
    if location != "None":
        loc = ET.SubElement(act, 'Location')
        loc.text = location
    ver = ET.SubElement(act, "Verify")
    ver.text = verify