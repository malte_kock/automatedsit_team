#import Typerkennen
#import csv
#from typ_code import typ1, typ3, typ4, typ5, typ6, typ7, typ8, typ9
import csv
from typ_code import typ1, typ3, typ4, typ5, typ6, typ7, typ8, typ9

csvFile = open("Warning_Caution_Advisory.csv", "r")
items = csv.reader(csvFile)

with open("Warning_Caution_Advisory_typ.csv", 'w') as r:
    csv_write = csv.writer(r)
    for (i, row) in enumerate(items):
        if row[0].startswith('Note') or row[0].startswith('NOTE') or row[0].startswith('note'):
            row.append('this is a note')
            csv_write.writerow(row)
            #print('this is a note:', i+1)
        elif typ1.analyse(row[0]):
            row.append('typ1: Listen_voice_tone')
            csv_write.writerow(row)
            #print('Listen_voice_tone')
        elif typ3.analyse(row[0]):
            row.append('typ3:Display_read')
            csv_write.writerow(row)
            #print('Display_read')
        elif typ4.analyse(row[0]):
            row.append('typ4:GUI')
            csv_write.writerow(row)
            #print('GUI')
        elif typ5.analyse(row[0]):
            row.append('typ5')
            csv_write.writerow(row)
            #print('typ5')
        elif typ6.analyse(row[0]):
            row.append('typ6:Type_press')
            csv_write.writerow(row)
            #print('Type_press')
        elif typ7.analyse(row[0]):
            row.append('typ7:Lamp')
            csv_write.writerow(row)
            #print('Lamp')
        elif typ8.analyse(row[0]):
            row.append('typ8: Wait')
            csv_write.writerow(row)
            #print('Wait')
        elif typ9.analyse(row[0]):
            row.append('typ9: Breaker')
            csv_write.writerow(row)
            #print('Breaker')
        else:
            row.append('unbekannte Typ')
            csv_write.writerow(row)
            #print('unbekannte Typ')



