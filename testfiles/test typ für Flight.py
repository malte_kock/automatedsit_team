import xml.etree.ElementTree as ET
import XML
from typ_code import typ1, typ3, typ4, typ5, typ6, typ7, typ8, typ9, typ10, typ11
import xlrd

#read excel file
workbook = xlrd.open_workbook("Flight_Plan.xlsx")             # open excel
#workbook = xlrd.open_workbook("Communication_System.xlsx")
data = workbook.sheets()[0]
nrows = data.nrows

# creat a root of xml-file
root = ET.Element('Test')

j = 0
text = []

for i in range(nrows):
    row = data.row_values(i)
    zeile = i+1

    if typ11.analyse(row[0]):
        j = i + 1
        while not data.row_values(j)[0].startswith('On CDU'):
            text.append(data.row_values(j)[0])
            j += 1
            if j == nrows:
                break
        print('Typ "waypoints_text": ', zeile)
        info = typ11.extract(row[0])
        line = ET.SubElement(root, 'line')
        line.text = str(zeile)
        XML.xml_waypoints_text(root, info['Device'], info['Location'], text)
        text = []

    elif i < j:
        pass

    else:
        pass


# save xml-file, name:'warning_caution1.xml'
tree = ET.ElementTree(root)
tree.write('Typ_waypoints_text.xml', encoding='utf-8', xml_declaration=True)      # name of xml
