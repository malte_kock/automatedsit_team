# Eine Test-Programm, um einzelne Typ zu überprüfen.
# Die Änderung liegt bei ‘if’ und Name der einzelne xml
# Wenn Alles in Ordnung und es Keinen Fehler gibt,und entsprechende xml geht gut,
# kann dieser Teil in Hautprogramm hingefügt werden.
import xml.etree.ElementTree as ET
import XML
import csv
from typ_code import typ1, typ3, typ4, typ5, typ6, typ7, typ8, typ9


# read csv file
csvFile = open("Warning_Caution_Advisory.csv", "r")
reader = csv.reader(csvFile)

# creat a root of xml-file
root = ET.Element('Test')

for (i, row) in enumerate(reader):
    zeile = i+1
    if typ6.analyse(row[0]):
        print('Typ "Type_press": ', zeile)
        info = typ6.extract(row[0])
        line = ET.SubElement(root, 'line')
        line.text = str(zeile)
        XML.xml_type_press(root, info['Device'], info['Action'], info['Text'], info['Button'], info['Select'],
                           info['Where'], info['Num_type_press'])

    else:
        pass


# save xml-file, name:'warning_caution1.xml'
tree = ET.ElementTree(root)
tree.write('Typ_cdu_verify_read.xml')      # name of xml


