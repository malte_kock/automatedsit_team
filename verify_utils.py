import variables_tables as vt
import re

class NoteException(Exception):
    # Exception for notes
    pass

class VerifyException(Exception):
    # Exception for failed verification
    pass

class NotImplementedException(Exception):
    # Exception for not implemented actions
    pass

class UserActionException(Exception):
    # Exception for user_actions
    pass


def verify_type(action_type):  # def verify_type(action_type, line_type):
    for variable in action_type:
        if variable.tag == "Device" and (variable.text not in vt.type_master["Device"]):
            raise VerifyException("Type: Unknown device.")

def verify_press(action_press):
    for variable in action_press:
        if variable.tag == "Device" and (variable.text not in vt.press_master["Device"]):
            raise VerifyException("Press: Unknown device.")

        if variable.tag == "Button" and (variable.text not in vt.press_master["Button"]):
            raise VerifyException("Press: Unknown button.")

        if variable.tag == "Switch" and (variable.text not in vt.press_master["Switch"]):
            raise VerifyException("Press: Unknown Switch.")

        if variable.tag == "Select" and (variable.text not in vt.press_master["Select"]):
            raise VerifyException("Press: Unknown Select.")

def verify_pull_push(action_push_pull):
    for variable in action_push_pull:
        if variable.tag == "Device" and (variable.text not in vt.pull_push_master["Device"]):
            raise VerifyException("Pull_Push: Unknown device.")

        if variable.tag == "Breaker" and (variable.text not in vt.pull_push_master["Breaker"]):
            raise VerifyException("Pull_Push: Unknown breaker.")

        if variable.tag == "Location" and (variable.text not in vt.pull_push_master["Location"]):
            raise VerifyException("Pull_Push: Unknown Location.")

def verify_listen_voice_tone(action_listen_voice_tone):
    for variable in action_listen_voice_tone:
        if variable.tag == "Device" and (variable.text not in vt.listen_voice_tone_master["Device"]):
            raise VerifyException("Verify_listen: Unknown device.")

        if variable.tag == "Tone_Voice" and (variable.text not in vt.listen_voice_tone_master["Tone_Voice"]):
            raise VerifyException("Verify_listen: Unknown tone or spoken bit.")

def verify_read(action_read):
    for variable in action_read:
        if variable.tag == "Device":
            temp = variable.text
            if type(temp) == str and temp not in vt.read_master["Device"]:
                raise VerifyException("Verify_read: Unknown device.")
            elif (type(temp)==list and not set(temp).issubset(set(vt.read_master["Device"]))):
                raise VerifyException("Verify_read: Unknown device.")

        if variable.tag == "Box" and (variable.text not in vt.read_master["Box"]):
            raise VerifyException("Verify_read: Unknown Box.")

        if variable.tag == "State" and (variable.text not in vt.read_master["State"]):
            raise VerifyException("Verify_read: Unknown State.")

        if variable.tag == "Text" and (variable.text not in vt.read_master["Text"]):
            raise VerifyException("Verify_read: Unknown Text.")

        if variable.tag == "Symbol" and (variable.text not in vt.read_master["Symbol"]):
            raise VerifyException("Verify_read: Unknown Symbol.")

        if variable.tag == "Location" and (variable.text not in vt.read_master["Location"]):
            raise VerifyException("Verify_read: Unknown Location.")

        if variable.tag == "Title" and (variable.text not in vt.read_master["Title"]):
            raise VerifyException("Verify_read: Unknown Title.")

def verify_waypoints(action_read):
    for variable in action_read:
        if variable.tag == "Device" and (variable.text not in vt.waypoints_master["Device"]):
            raise VerifyException("Verify_read: Unknown Device.")

        if variable.tag == "Location" and (variable.text not in vt.waypoints_master["Location"]):
            raise VerifyException("Verify_read: Unknown Location.")

def verify_check(action_check):
    for variable in action_check:
        if variable.tag == "Device" and (variable.text not in vt.check_master["Device"]):
            raise VerifyException("Verify_check_lamp: Unknown device.")

        if variable.tag == "Lamp" and (variable.text not in vt.check_master["Lamp"]):
            raise VerifyException("Verify_check_lamp: Unknown Lamp.")

        if variable.tag == "State" and (variable.text not in vt.check_master["State"]):
            raise VerifyException("Verify_check_lamp: Unknown State.")

        if variable.tag == "Location" and (variable.text not in vt.check_master["Location"]):
            raise VerifyException("Verify_check_lamp: Unknown Location.")

def verify_wait(action_wait):
    for variable in action_wait:
        if variable.tag == "Time":
            if not variable.text.isdigit():
                raise VerifyException("Verify_wait: Unknown Time.")

def verify_set(action_set):
    for variable in action_set:
        if variable.tag == "Device" and (variable.text not in vt.set_master["Device"]):
            raise VerifyException("Verify_set: Unknown device.")

        if variable.tag == "Interface" and (variable.text not in vt.set_master["Interface"]):
            raise VerifyException("Verify_set: Unknown interface.")

        if variable.tag == "Control_box" and (variable.text not in vt.set_master["Control_box"]):
            vt.help_table.append(variable.text)
            raise VerifyException("Verify_set: Unknown Control box.")

        if variable.tag == "State" and (variable.text not in vt.set_master["State"]) and not checkIfUnit(variable.text):
            raise VerifyException("Verify_set: Unknown State.")

def checkIfUnit(string):

    string = string.strip()
    unit = string.split(" ", 1)[-1]
    if unit in vt.units:
        return True
    else:
        return False
