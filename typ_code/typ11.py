# analyseTyp1 gibt einen Wert zurück der so aussieht: device,  location (wie in Metasprache definiert)
# On CDU #2, verify the following waypoints are displayed in the active flight plan.
# device: CDU #2
# location: active flight plan


import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ11(string):
    match1 = re.search(r'(?<=On\s).*?(?=,)', string)
    match2 = re.search(r'\s*verify the following\s*\w*\s*waypoints are (displayed|contained)\s*', string)
    match3 = re.search(r'(?<=(i|o)n the\s).*', string[match2.end():])

    device = match1.group()
    location = re.sub(r'[,.?:`!\":\']', '', match3.group())

    return device, location


# implementation of analyse(string)
# returns 1 if string matches type 4, else returns 0
def analyse(string):
    try:
        typ11(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 4
def extract(string):
    device, location = typ11(string)
    dic = {
        "Device": device,
        "Location": location
    }
    return dic

'''
#string = 'On CDU #2, verify the following waypoints are displayed in the active flight plan.'
#string = 'On CDU #2, verify the following SID waypoints are displayed in the active flight plan.'
string = 'On CDU #1, verify the following waypoints are contained on the LEGS Page"'
match1 = re.search(r'(?<=On\s).*?(?=,)', string)
print(match1)
match2 = re.search(r'\s*verify the following\s*\w*\s*waypoints are (displayed|contained)\s*', string)
match3 = re.search(r'(?<=(i|o)n the\s).*', string[match2.end():])

device = match1.group()
location = re.sub(r'[,.?:`!\":\']', '', match3.group())
print(device)
print(location)
'''

