# analyseTyp3 gibt einen Wert zurück der so aussieht: device, symbol, text, col(color), bcol(backgroundcolor), verify,box, state
# On MFDs 2, 5 & 3; verify the ENG 1 "FIRE" warning is (/not) displayed in Black Text on a Red Background.;Verify
# device: MFD2/...
# symbol: ENG 1
# text:'FIRE'
# col: Black
# bcol:Red
# verify: yes(/no)

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ_display_read(string):

    # bug fix issue #26 - get caution/advisory information
    notification_type = re.search(r'(A|a)dvisory|(C|c)aution|(W|w)arning', string)

    match1 = re.search(r'MFDs?\s*', string)
    match2 = re.search(r'verify the\s*', string)
    match4 = re.search(r'\w*(?=\s*(T|t)ext\s*)', string)
    match5 = re.search(r'on.*Background', string)

    # state = ''
    if '1' in string[:match2.start()] or '4' in string[:match2.start()]:
        verify = 'displayed'
        str2 = string[match2.end():]
        match7 = re.search(r'\s*is\s*', str2)
        box = str2[:match7.start()]
        match8 = re.search('\s*and\s(the)?\s*', str2)
        if match8 is not None:
            state = str2[match7.end(): match8.start()]
            match9 = re.search(r'\s*(S|s)ymbols?\s*(is|are)\s*', str2)
            symbol = str2[match8.end():match9.start()]
            match3 = re.search(r'\"(.*)?\"', string)
            if match3 is not None:
                text = match3.group()
            else:
                match10 = re.search(r'\w*', str2[match9.end():])
                state = state.split()
                state.append(match10.group())
        else:
            state = str2[match7.end():].strip('.')


    else:
        box = 'None'
        state = 'None'
        match3 = re.search(r'\"(.*)?\"', string)
        text = match3.group()
        symbol = string[match2.end(): match3.start()].strip()

        match6 = re.search(r'displayed\s*', string)
        if 'not' in string[match6.start() - 5: match6.start()] or 'NOT' in string[match6.start() - 5: match6.start()]:
            verify = 'not displayed'
        else:
            verify = 'displayed'

        match11 = re.search(r'(S|s)econds?', string)
        if match11 is not None:
            time = string[match6.end():match11.end()]
            time = re.search(r'\d+', time).group()

    mfd_num = [char for char in string[match1.end():match2.start()] if char.isdigit()]
    num = len(mfd_num)
    device = []
    for i in range(num):
        device.append('MFD#' + mfd_num[i])

    if 'symbol' in dir():
        pass
    else:
        symbol = 'None'
    if 'text' in dir():
        pass
    else:
        text = 'None'

    if 'time' in dir():
        pass
    else:
        time = 'None'

    if notification_type is not None:
        notification_type = notification_type.group(0)
    else:
        notification_type = 'None'

    if match4 is not None:
        col = match4.group()
    else:
        col = 'None'

    if match5 is not None:
        bcol = match5.group().split()[-2]
    else:
        bcol = 'None'

    if box == 'LDG status box' and re.match(r'(a|A)ll (T|t)hree', symbol):
        symbol = "all three LDGs"

    return device, symbol, text, col, bcol, verify, box, state, time, notification_type


# implementation of analyse(string)
# returns 1 if string matches type 3, else returns 0
def analyse(string):
    try:
        typ_display_read(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 3
def extract(string):
    device, symbol, text, col, bcol, verify, box, state, time, notification_type = typ_display_read(string)
    dic = {
        "Device": device,
        "Symbol": symbol,
        "Text": text,
        "Col": col,
        "Bcol": bcol,
        "Verify": verify,
        "Box": box,
        "State": state,
        "Time": time,
        "Notification_type": notification_type
    }
    return dic

'''
# test for correct
#string = 'On MFDs 2, 5 & 3; verify the ENG 1 "ENGPLO" warning is  not  displayed.;Verify'
#string = 'On MFDs 2, 5 & 3; verify the ENG 1 "FIRE" warning is (/not) displayed in Black Text on a Red Background.;Verify'
#string = 'On MFDs 1 & 4; verify the LDG status box is not displayed.'
#string = 'On MFDs 1 & 4; verify the LDG status box is Drawn and the Nose and Right LDG Symbols are "\\" Black Text on an Amber Background.  (indicating nose gear is In Transition and right gear has a Switch Failure)'
#string = 'On MFDs 1 & 4; verify the LDG status box is Drawn and the Lower Right Symbol is "\\" Black Text on an Amber Background.  (indicating right gear in transition)'
#string = 'On MFDs 1 & 4; verify the LDG status box is Blank.'
#string = '??497.On MFDs 1 & 4; verify the LDG status box is Drawn and the All Three LDG Symbols are "\\" Black Text on an Amber Background.  (indicating gear is In Transition)'
string = '512.On MFDs 1 & 4; verify the LDG status box is Drawn and the Left LDG Symbols is Blank(indicating gear Up and Locked)'

match1 = re.search(r'MFDs?\s*', string)
match2 = re.search(r'verify the\s*', string)
match4 = re.search(r'\w*(?=\s*(T|t)ext\s*)', string)
match5 = re.search(r'on.*Background', string)
#state = ''
if '1' in string[:match2.start()] or '4' in string[:match2.start()]:
    verify = 'yes'
    str2 = string[match2.end():]
    match7 = re.search(r'\s*is\s*', str2)
    box = str2[:match7.start()]
    match8 = re.search('\s*and\s(the)?\s*', str2)
    if match8 is not None:
        state = str2[match7.end(): match8.start()]
        match9 = re.search(r'\s*(S|s)ymbols?\s*(is|are)\s*', str2)
        symbol = str2[match8.end():match9.start()]
        match3 = re.search(r'\"(.*)?\"', string)
        if match3 is not None:
            text = match3.group()
        else:
            match10 = re.search(r'\w*', str2[match9.end():])
            state = state.split()
            state.append(match10.group())
    else:
        state = str2[match7.end():].strip('.')


else:
    box = 'None'
    state = 'None'
    match3 = re.search(r'\"(.*)?\"', string)
    text = match3.group()
    symbol = string[match2.end(): match3.start()]

    match6 = re.search(r'displayed\s*', string)
    if 'not' in string[match6.start() - 5: match6.start()] or 'NOT' in string[match6.start() - 5: match6.start()]:
        verify = 'no'
    else:
        verify = 'yes'

    match11 = re.search(r'(S|s)econds?', string)
    if match11 is not None:
        time = string[match6.end():match11.end()]

mfd_num = [char for char in string[match1.end():match2.start()] if char.isdigit()]
num = len(mfd_num)
device = []
for i in range(num):
    device.append('MFD#' + mfd_num[i])


if 'symbol'in dir():
    pass
else:
    symbol = 'None'
if 'text' in dir():
    pass
else:
    text = 'None'
if 'time' in dir():
    pass
else:
    time = 'None'


if match4 is not None:
    col = match4.group()
else:
    col = 'None'

if match5 is not None:
    bcol = match5.group().split()[-2]
else:
    bcol = 'None'


print('********************************************')

print(device)
print(symbol)
print(text)
print(col)
print(bcol)
print(box)
print(state)
print(verify)
'''

