# analyseTyp3 gibt einen Wert zurück der so aussieht: device, action, text, button, select, where, num_type_press
# On the CSE Computer, type START_Nsis() and press Enter
# device: CSE Computer
# action: ["type", "press"]
# text: START_Nsis()
# button: Enter
# select: "None"
# where: "None"
# num_type_press: < 0

import re


def typ6(string):
    match1 = re.search(r'(?<=(I|O)n )(the)?.*?(?=\s*,)', string)
    match2 = re.search(r'"?\s?(and|to)?\s+?(type|enter)\s*', string)
    match3 = re.search(r'"?\s?(and)?\s?press\s*(the)?\s*', string)

    device_str = match1.group().lstrip('the ')
    if 'CDU' in device_str:
        num_device = [char for char in device_str if char.isdigit()]
        device = []
        for i in range(len(num_device)):
            device.append('CDU #' + num_device[i])
    else:
        device = [device_str]
    action = []
    select = 'None'
    #position = 'None'
    num_type_press = 0
    if match2 is not None and match3 is not None:
        num_type_press = match2.start() - match3.start()  # press first: num > 0     enter/type first: num < 0
        action.append("Type")
        action.append("Press")
        if num_type_press < 0:
            text = string[match2.end():match3.start()].strip(' ')
            match7 = re.search(r'\s+in\s*the\s*scratchpad', text)
            if match7 is not None:
                text = text[:match7.start()]
            button = string[match3.end():].strip('.')
        else:
            button = string[match3.end():match2.start()]
            text = string[match2.end():].split()[0].strip(' ')

    elif match2 is not None:
        button = 'None'
        action.append("Type")
        match4 = re.search(r'\s*into\s(the)?\s*', string)
        if match4 is not None:
            text = string[match2.end():match4.start()].strip(' ')
            #position = string[match4.end():].strip('.')
        else:
            text = string[match2.end():].strip('.')

    else:
        text = 'None'
        action.append("Press")
        button = string[match3.end():].split()[0]
        match5 = re.search(r'\s*(to)?\s*select\s*', string)
        match6 = re.search(r'button\s*?\<.*?\>', string[match3.end():])
        match8 = re.search(r'\<.*?\>\s*?button', string[match3.end():])
        if match5 is not None:
            select = string[match5.end():].split()[0].strip(' .')
            match9 = re.search('\".*\"', string[match5.end():])
            if match9 is not None:
                select = match9.group().replace(' ', '')
        if match6 is not None:
            button = re.search(r'\<.*?\>', match6.group()).group().replace(' ', '')
        elif match8 is not None:
            button = re.search(r'\<.*?\>', match8.group()).group().replace(' ', '')

    button = button.strip(' ')
    if re.search(r'button', button):
        button = re.sub(r'button', '', button).replace(' ', '')
    return device, action, text, button, select, num_type_press


def analyse(string):
    try:
        typ6(string)
        return 1
    except AttributeError:
        return 0


def extract(string):
    device, action, text, button, select, num_type_press = typ6(string)
    keys = ["Device", "Action", "Text", "Button", "Select", "Num_type_press"]
    variables = [device, action, text, button, select, num_type_press]
    diction = dict(zip(keys, variables))
    return diction


'''
#string = 'On  CDU #1 , press  <CLEAR>  button  <LS5R>'
#string = 'On the CSE Computer, when prompted, type  N4146.754W08744.624  as the present position and press Enter.'
#string = 'On MFDs #1 & 4, press  B1, L2  to access PFD ARC.'
#string = 'On the CSE Computer, type  1  and press Enter.'
#string = 'On CDU #1, press  L1  to enter KMDW as ORIGIN.'
#string = 'On CDU #2, press  L1  START INIT to access the START INIT 1/3 page.'
#string = 'On CDU #1, press  R3  to select RW13C.'
#string = 'On CDU #1, press  EXEC  to execute the Modified Flight Plan.'
#string = 'On CDU #1, press  ↑ .'
#string = 'On CDU #1, press  DP/AR  twice to access the DEP/ARR INDEX page.'
#string = 'On CDU #2, enter "N4146.754W08744.624" into the scratchpad.'
#string = 'On CDU #1, enter "KMDW" into the scratchpad.'
#string = 'In the CSE scripting window, type "nsis.afcs_2.disc_apm2_bmapa = 1"'
#string = 'On the CSE Computer, type  import sil_startup  and press Enter.'
#string = 'On the center console, press the "Pitch Down Transition" button.'
#string = 'On CDU 1 on the  ES Control page, press R5 to reset the MISFIRE condition.'
#string = 'On CDU #2, enter GSH in the scratchpad and press  R2 .'
string = 'On  CDU #1 and CDU #2 , press the < COM>  button to access the top level  "COM"  pages'
#string = 'On  CDU #1 , press  < LS1R > button'
match1 = re.search(r'(?<=(I|O)n )(the)?.*?(?=,)', string)
print(match1)
device = match1.group().lstrip('the ')
match2 = re.search(r'"?\s?(and|to)?\s+?(type|enter)\s*', string)
print(match2)
match3 = re.search(r'"?\s?(and)?\s?press\s*', string)
print(match3)
action = []
select = 'None'
where = 'None'
if match2 is not None and match3 is not None:
    num_type_press = match2.start() - match3.start()   # press first: num > 0     enter/type first: num < 0
    print(num_type_press)
    action.append("type")
    action.append("press")
    if num_type_press < 0:
        text = string[match2.end():match3.start()]
        print(text)
        match7 = re.search(r' in the scratchpad', text)
        print(match7)
        button = string[match3.end():].strip('.')
    else:
        button = string[match3.end():match2.start()]
        text = string[match2.end():].split()[0]


elif match2 is not None:
    button = 'None'
    action.append("type")
    match4 = re.search(r'\s*into\s(the)?\s*', string)
    print(match4)
    if match4 is not None:
        text = string[match2.end():match4.start()]
        where = string[match4.end():].strip('.')
        print(where)
    else:
        text = string[match2.end():].strip('.')

elif match3 is not None:
    print('**************')
    text = 'None'
    action.append("press")
    temp_str = string[match3.end():]
    button = temp_str.split()[0]
    print('temp:',string[match3.end():])
    #print(button)
    #if re.search(r'^<\w+', string[match3.end():]) and not re.search(r'^<\w+>', string[match3.end():]):
        #button = button + '>'
    match5 = re.search(r'\s*to\s*select\s*', string)
    if match5 is not None:
        select = string[match5.end():].split()[0]

    match6 = re.search(r'\sbutton\s*\<.*?\>', temp_str)
    print('mat6:',match6)
    match8 = re.search(r'\<.*?\>\s*button', temp_str)
    print('mat8:', match8)
    if match6 is not None:
        button = re.search(r'\<.*?\>', match6.group()).group().replace(' ', '')
        #print(button)
    elif match8 is not None:
        button = re.search(r'\<.*?\>', match8.group()).group().replace(' ', '')

print('*******************************')
print(device)
print(action)
print(text)
print(button)
print(select)
print(where)
'''