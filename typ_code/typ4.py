#analyseTyp1 gibt einen Wert zurück der so aussieht: device, interface, schaltfläche, state (wie in Metasprache definiert)
# Using the CSE "DCU INPUTS 1" Main GUI, set "Eng 1 Fire" to Active (Green).
# device: CSE
# interface: "DCU_INPUTS_1" Main GUI
# schaltfläche: Eng 1 Fire
# state: Active (Green)

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ4(string):
    match1 = re.search(r'(Using|On)\s*(the)?\s*\W', string)
    match2 = re.search(r', set\s*(the)?\s*.*?\"', string)
    match3 = re.search(r'\"?\s+(to|=) ', string)
    match4 = re.search(r', ((a|A)ctivate|(d|D)eactivate)\s*(\(.*\))?\s*', string)
    if match2 is not None:
        control_box = string[match2.end():match3.start()]
        state = string[match3.end():].strip(' .')
        match8 = re.search(r'.*(?=\(.*\))', string[match3.end():])
        if match8 is not None:
            state = match8.group().strip(' .')

        match7 = re.search(r'.*?(?=\s*\(((G|g)reen|not green)\))', state)

        # Clean State if it contains a unit: 200 kgf/cm 2 .  (standard_setup) -> 200 kgf/cm 2
        match_unit = re.search(r'.*(?= \.  \(standard)', state)
        if match_unit is not None:
            state = match_unit.group()

        if match7 is not None:
            state = match7.group()
        target = string[match1.end():match2.start()]
        match5 = re.search(' ', target)
        device = target[0:match5.start()]
        interface = target[match5.end():]

    else:
        target = string[match1.end():match4.start()]
        match5 = re.search(' ', target)
        device = target[0:match5.start()]
        interface = target[match5.end():]
        if 'activate' in match4.group().split():
            match6 = re.search(r'\sto\s', string)
            control_box = string[match4.end():match6.start()]
            state = 'Active'
        else:
            control_box = string[match4.end():].strip('.')
            state = 'Inactive'
    match9 = re.search(r'(?<=^On\s)\s*SIL Bench', string)
    #if match9 is not None:
        #device = match9.group().lstrip(' ')
        #interface = 'None'

    return device, interface, control_box, state

# implementation of analyse(string)
# returns 1 if string matches type 4, else returns 0
def analyse(string):
    try:
        typ4(string)
        return 1
    except AttributeError:
        return 0

# implementation of extract(string)
# returns dictionary with elements of instruction of type 4
def extract(string):
    device, interface, control_box, state = typ4(string)
    temp = {
        "Device": device,
        "Interface": interface,
        "Control_box": control_box,
        "State": state
    }
    return temp


'''
#string = 'Using the CSE "radar_altimeter_1_gui" GUI, set the Radar "Altitude Bias" to 151 ft.'
string = 'Using the CSE "DCU INPUTS 1" Main GUI, set "Eng 1 Fire" to Active (Green).'
match1 = re.search('(Using|On)\s*(the)?\s*\W', string)
match2 = re.search(r', set\s*(the)?\s*.*?\"', string)
print(match2)
match3 = re.search('"? (to|=) ', string)
print(match3)
match4 = re.search(r', ((a|A)ctivate|(d|D)eactivate)\s*(\(.*\))?\s*', string)
if match2 is not None:
    schaltfläche = string[match2.end():match3.start()]
    state = string[match3.end():].strip('.')
    match7 = re.search(r'.*?(?=\s*\((Green|not green)\))', state)
    if match7 is not None:
        state = match7.group()
    target = string[match1.end():match2.start()]
    match5 = re.search(' ', target)
    device = target[0:match5.start()]
    interface = target[match5.end():]

else:
    target = string[match1.end():match4.start()]
    match5 = re.search(' ', target)
    device = target[0:match5.start()]
    interface = target[match5.end():]
    if 'activate' in match4.group().split():
        match6 = re.search(r'\sto\s', string)
        schaltfläche = string[match4.end():match6.start()]
        state = 'active'
    else:
        schaltfläche = string[match4.end():].strip('.')
        state = 'inactive'

print(schaltfläche)
print(state)
'''
