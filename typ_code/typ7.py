# analyseTyp7 gibt einen Wert zurück der so aussieht: device, lamp, state
# On the CWA Panel, verify the "CHIP" Lamp is On.
# device: CWA Panel
# lamp: CHIP
# state: on

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ_lamp(string):
    match1 = re.search(r'(?<=On the\s).*(?=,)', string)
    match2 = re.search(r'(?<=verify the\s).*(?=\s+Lamp)', string)
    match3 = re.search(r'(?<=(L|l)amp)\s*\w*\s*\w*', string)
    if 'flash' in match3.group():
        state = match3.group().lstrip()
    else:
        state = match3.group().split()[-1]

    lamp = match2.group().strip(' ')
    location = 'None'
    if len(match2.group().split()) > 1:
        if 'Family' in match2.group():
            pass
        else:
            match4 = re.search(r'\".*\"', match2.group())
            lamp = match4.group()
            location = match2.group()[:match4.start()].strip(' ')

    device = match1.group()

    return device, lamp, location, state

# implementation of analyse(string)
# returns 1 if string matches type 3, else returns 0
def analyse(string):
    try:
        typ_lamp(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 3
def extract(string):
    device, lamp, location, state = typ_lamp(string)
    dic = {
        "Device": device,
        "Lamp": lamp,
        "State": state.capitalize(),
        "Location": location
    }
    return dic


'''
#string = 'On the CWA Panel, verify the Family "AVCS" Lamp is On.'
#string = '??1151. On the CWA Panel, verify the Family "AVCS" Lamp flashes once.'
string = 'On the CWA Panel, verify the "LDGUP" Lamp remains OFF.'
match1 = re.search(r'(?<=On the\s).*?(?=\s*,)', string)
print(match1)
#match2 = re.search(r'\"(.*)\"(?=\s+(L|lamp))', string)
#print(match2)
match3 = re.search(r'(?<=(L|l)amp)\s*\w*\s*\w*', string)
print(match3)
if 'flash' in match3.group():
    state = match3.group().lstrip()
else:
    state = match3.group().split()[-1]
print(state)
match4 = re.search(r'(?<=verify the\s).*(?=\s+Lamp)', string)
print(match4)
if len(match4.group().split()) > 1:
    if 'Family' in match4.group():
        pass
    else:
        match5 = re.search(r'\".*\"', match4.group())
        print(match5)
        lamp = match5.group()
        location = match4.group()[:match5.start()].strip(' ')        
'''

