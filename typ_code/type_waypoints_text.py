# Die Typ erkennt den nachfolgende Text von waypoints.
# Diese Text hat ordentliche Form: "SHOEY   	STAR   	   	TF   	-   	-   	309°   	41.4NM"
# in Python wegen encoding wird so gezeigt wird:
# 'SHOEY\xa0\xa0\xa0\tSTAR\xa0\xa0\xa0\t\xa0\xa0\xa0\tTF\xa0\xa0\xa0\t-\xa0\xa0\xa0\t-\xa0\xa0\xa0\t309°\xa0\xa0\xa0\t41.4NM'


import re


def type_waypoints_text(string):
    match1 = re.search(r'^.*((\xa0)+\t.*)+', string)
    datenzeile = match1.group()

    return datenzeile

def analyse(string):
    try:
        type_waypoints_text(string)
        return 1
    except AttributeError:
        return 0


def extract(string):
    datenzeile = type_waypoints_text(string)
    dic = {'Waypoint': datenzeile}
    return dic

'''
#string = "SHOEY   	STAR   	   	TF   	-   	-   	309°   	41.4NM"
#string = "Wpt   	   	Proc   	   	Leg   	Alt   	Spd   	Bearing   	Distance"
#string = "DISCON   	-   	   	DI   	-   	-   	-   	-"
#string = "RW06R   	   	SID"
string = "(526)   	   	   	SID"
#string = "BERKS   	   	   	Apr (trans)"
#string = "BSR   	   	   	STAR"
#string = "Wpt"
#string = 'On CDU #1,  FPLN  to access the Active Flight Plan 1/X Page.'
#string = "BRI JJ   	   	MissedApr   	DF   	2200A   	-   	-   	6.1NM"
#string = 'OAK        -    -    -    -    -'
#string = "Wpt        Proc        Leg    Alt    Spd    Bearing    Distance"

match1 = re.search(r'^.*((\xa0|\s)+\t.*)+', string)
print(match1)

#  'Wpt\xa0\xa0\xa0\t\xa0\xa0\xa0\tProc\xa0\xa0\xa0\t\xa0\xa0\xa0\tLeg\xa0\xa0\xa0\tAlt\xa0\xa0\xa0\tSpd\xa0\xa0\xa0\tBearing\xa0\xa0\xa0\tDistance'
'''
