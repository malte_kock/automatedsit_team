# analyseTyp12 gibt einen Wert zurück der so aussieht: device, page, symbol, text, location, verify
# On  CDU #1 , verify if the  "DEFAULT fixed frequency""243.000AM" MHz is displayed in data line 1
# device: CDU #1
# page: "None"
# symbol: "DEFAULT fixed frequency"
# text: "243.000AM" MHz
# location: data line 1
# verify: yes

import re


def typ12(string):
    match1 = re.search(r'(?<=^On )\s*CDU.*?(?=\s*,)', string)
    match2 = re.search(r'verify if (the)?.* is displayed', string)
    match3 = re.search(r'\".*?\"\s*\".*?\"', match2.group())
    device_str = match1.group().lstrip(' ')
    num_device = [char for char in device_str if char.isdigit()]
    device = []
    for i in range(len( num_device)):
        device.append('CDU #' + num_device[i])

    page = 'None'
    symbol = 'None'
    text = 'None'
    location = 'None'
    if match3 is not None:
        match5 = re.search(r'\".*?\"', match3.group())
        symbol = match5.group()
        text = match3.group()[match5.end():].strip(' .')
    else:
        match4 = re.search(r'\".*?\"(?=\s*page)', match2.group())
        if match4 is not None:
            page = match4.group()
        else:
            text = re.search(r'\".*?\"', match2.group()).group().replace(' ', '')

    match6 = re.search(r'in.*line\s\d*', string[match2.end():])
    if match6 is not None:
        location = match6.group().lstrip('in ')

    return device, page, symbol, text, location


def analyse(string):
    try:
        typ12(string)
        return 1
    except AttributeError:
        return 0


def extract(string):
    device, page, symbol, text, location = typ12(string)
    dic = {
        "Device": device,
        "Page": page,
        "Symbol": symbol,
        "Text": text,
        "Location": location,
        "Verify": 'yes'
    }
    return dic

'''
#string = 'On  CDU #2 , verify if the  "POWER CONTROL"  page is displayed'
#string = 'On  CDU #1 , verify if  "V1√" "OFF "  is displayed in data line 1'
#string = 'On  CDU #1 , verify if the  "DEFAULT fixed frequency" "243.000AM" MHz i s displayed in  data line 1'
#string = 'On  CDU #1 , verify if   "DOMAIN "  is displayed in  label line 3'
#string = 'On  CDU #1 , verify if for  "DOMAIN"   "0"  is displayed in  data line 3'
string = 'On  CDU #1 , verify if for  "WOD"   " TRAINING"  is displayed in  data line 3'
match1 = re.search(r'(?<=^On )\s*CDU.*?(?=\s*,)', string)
print(match1)
device = match1.group().lstrip(' ')
match2 = re.search(r'verify if (the)?.* is displayed', string)
print(match2)
match3 = re.search(r'\".*?\"\s*\".*?\"', match2.group())
print(match3)

page = 'None'
symbol = 'None'
text = 'None'
location = 'None'
if match3 is not None:
    match5 = re.search(r'\".*?\"', match3.group())
    print(match5)
    symbol = match5.group()
    text = match3.group()[match5.end():].lstrip(' ')
else:
    match4 = re.search(r'\".*?\"(?=\s*page)', match2.group())
    print(match4)
    if match4 is not None:
        page = match4.group()
    else:
        text = re.search(r'\".*?\"', match2.group()).group()


match6 = re.search(r'in.*line\s\d*', string[match2.end():])
print(match6)
if match6 is not None:
    location = match6.group().lstrip('in ')

print('********************')
print(device)
print(page)
print(symbol)
print(text)
print(location)
'''

