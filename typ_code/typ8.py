# analyseTyp7 gibt einen Wert zurück der so aussieht: time
# Wait 7 seconds.Text
# time: 7

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ_wait(string):
    match1 = re.search(r'(W|w)ait.*(S|s)econd', string)
    match2 = re.search(r'\d+', match1.group())
    time = match2.group()
    return time


# implementation of analyse(string)
# returns 1 if string matches type 3, else returns 0
def analyse(string):
    try:
        typ_wait(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 3
def extract(string):
    time = typ_wait(string)
    dic = {
        "Time": time
    }
    return dic

'''
string = 'Wait 71 seconds.Text'
match1 = re.search(r'(W|w)ait.*(S|s)econd', string)
match2 = re.search(r'\d+', match1.group())
time = match2.group()
print(time)
'''