#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 12:35:48 2020

@author: yangnan
"""

import re


def typ5(string):
    result1 = re.search(r'on the Console\s*\w*\s*\w*', string)
    result2 = re.search(r'"? type ', string)
    result3 = re.search(r'type.*and', string)
    result4 = re.search(r'Press.*on\s', string)
    result5 = re.search(r'set\s*the?\s*"?', string)

    result6 = re.search(r'(?<=In the\s).*(?=,)', string)
    str = ' '

    if result4 is not None:
        action = result4.group().split()[0]
        #       type=str.join(result3.group().split()[1:-1])
        #       press=string[result5.end():].strip('.')
        device = str.join(result1.group().split()[2:])
        switch = str.join(result4.group().split()[1:-1]).lstrip('the ')
    else:
        action = "Set"
        switch = string[result5.end():result1.start()].lstrip('the ')
        device = str.join(result1.group().split()[2:])

    return action, device, switch


def analyse(string):
    try:
        typ5(string)
        return 1
    except AttributeError:
        return 0


def extract(string):
    action, device, switch = typ5(string)
    keys = ["Action", "Device", "Switch"]
    variables = [action, device, switch]
    diction = dict(zip(keys, variables))
    return diction


'''#Anweisung von typ6
string1="Momentarily set the FIRE ACK Switch on the Console Switch Panel.;Text"
string2="Press the Copilot M/C RESET GRIP PNL Switch on the Console Switch Panel."
string3="Momentarily set the FIRE ACK Switch on the Console Switch Panel "

#testing
string=string1
#print(extract(string))

  


'''

    

