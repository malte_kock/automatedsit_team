# analyseTyp1 gibt einen Wert zurück der so aussieht: device, text, location, file, title, symbol (wie in Metasprache definiert)
# On CDU #2, verify the secondary flight plan is saved as a modified flight plan ("RW13C" is shown at R3 and "MOD" is in the title)
# device: CDU #2
# text: "RW13C"
# location: 'None'
# file: modified flight plan
# title: "MOD"
# symbol: R3

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ10(string):
    match1 = re.search(r'(?<=On\sCDU).*?(?=,)', string)
    match2 = re.search(r'(?<=verify\s).*(?=\s*is displayed)', string)
    device = 'CDU' + match1.group()

    location = 'None'
    symbol = 'None'
    text = 'None'
    file = 'None'
    title = 'None'

    if match2 is not None:
        match3 = re.search(r'(\"|\').*?(\"|\')', string[match2.start():])
        match4 = re.search(r'\s(i|o)n\s*(the)?\s*', string[match2.end():])
        if match3 is not None:
            text = match3.group().strip(' ')
            #text = match3.group().split()[0].strip(',')
        else:
            text = match2.group().strip(' ')
            text = text.lstrip('the ')
        if match4 is not None:
            location = string[match2.end():][match4.end():].strip('.')
    else:
        match5 = re.search(r'verify.*saved as\s*?an?\s*', string)
        match6 = re.search(r'\s*?\(.*\)', string)
        if match6 is not None:
            file = string[match5.end():match6.start()]
            match7 = re.search(r'\s*(is)?\s*in (the)?\s*title', match6.group())
            title = match6.group()[:match7.start()].split()[-1].lstrip('( ')
            match8 = re.search(r'\s*is shown at\s*', match6.group())
            if match8 is not None:
                text = match6.group()[:match8.start()].split()[0].strip('(')
                symbol = match6.group()[match8.end():].split()[0]

        else:
            file = string[match5.end():].strip('.')

    return device, text, location, file, title, symbol


# implementation of analyse(string)
# returns 1 if string matches type 4, else returns 0
def analyse(string):
    try:
        typ10(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 4
def extract(string):
    device, text, location, file, title, symbol = typ10(string)
    dic = {
        "Device": device,
        "Text": text,
        "Location": location,
        "File": file,
        "Title": title,
        "Symbol": symbol
    }
    return dic


'''
#string = 'On CDU #1, verify "INSERT RTK BEFORE?" message is displayed in the scratchpad.'
#string = 'On CDU #1, verify ACT is displayed on the Title Line.'
#string = 'On CDU #1, verify KJFK is displayed as DEST.'
string = "On CDU #1, verify KMDW, Midway Airport's four-letter ID, is displayed as ORIGIN."
#string = 'On CDU #2, verify the secondary flight plan is saved as a modified flight plan ("RW13C" is shown at R3 and "MOD" is in the title).'
#string = 'On CDU #2, verify modified active flight plan is now saved as an active flight plan ("MOD" changes to "ACT" in title).'

match1 = re.search(r'(?<=^On\s).*?(?=,)', string)
print(match1)
device = match1.group()
match2 = re.search(r'(?<=verify\s).*(?=\s*is displayed)', string)
print(match2)

location = 'None'
symbol = 'None'
text = 'None'
file = 'None'
title = 'None'
if match2 is not None:
    match3 = re.search(r'(\"|\').*?(\"|\')', string[match2.start():])
    print(match3)
    match4 = re.search(r'\s*(i|o)n\s*(the)?\s*', string[match2.end():])
    print(match4)
    if match3 is not None:
        text = match3.group()
    else:
        text = match2.group().split()[0].strip(',')

    if match4 is not None:
        location = string[match2.end():][match4.end():].strip('.')
else:
    match5 = re.search(r'verify.*saved as\s*?an?\s*', string)
    print(match5)
    match6 = re.search(r'\s*?\(.*\)', string)
    print(match6.group())
    if match6 is not None:
        file = string[match5.end():match6.start()]
        match7 = re.search(r'\s*(is)?\s*in (the)?\s*title', match6.group())
        print(match7)
        title = match6.group()[:match7.start()].split()[-1]
        print(title)
        match8 = re.search(r'\s*is shown at\s*', match6.group())
        print(match8)
        if match8 is not None:
            text = match6.group()[:match8.start()].split()[0].strip('(')
            symbol = match6.group()[match8.end():].split()[0]

    else:
        file = string[match5.end():].strip('.')

print('*****************************')
print(device)
print(text)
print(location)
print(file)
print(title)
print(symbol)
'''
