import re


def typ1(string):
    match1 = re.search(r'Warning Tone Generator', string)
    str_temp = string[match1.end():]
    match2 = re.search(r'produces a Chime Tone & Voice .*', str_temp)
    match4 = re.search(r' repeats ', str_temp)
    match5 = re.search(r'(\sNOT\s|\sstops\s)', str_temp)
    match6 = re.search(r'\sproduc.*\sTone', str_temp)
    if match5 is not None:
        voice = 'None'
        tone = 'None'
    elif match4 is not None:
        voice = 'repeat last voice'
        tone = 'repeat last tone'
    elif match2 is not None:  # warnings, chime is missing
        voice = re.search(r'\".*\"', match2.group())
        voice = voice.group()
        tone = "Chime Tone"  # this is valid, since every voice warning is lead by chime tone.
    elif match6 is not None:  # special tones
        match7 = re.search(r'\s(a|the)\s+', match6.group())
        voice = 'None'
        tone = match6.group()[match7.end():]
    else:
        raise AttributeError

    return voice, tone


def analyse(string):
    try:
        typ1(string)
        return 1
    except AttributeError:
        return 0


def extract(string):
    voice, tone = typ1(string)
    dic = {
        "Device": "Warning Tone Generator",
        "Action": "Listen",
        "Tone": tone,
        "Voice": voice
        # "Verify": "yes"
    }
    return dic

'''
#string = 'Verify the Warning Tone Generator produces a Chime Tone & Voice "FIRE ENGINE ONE FIRE".;Verify'
#string = 'Verify the Warning Tone Generator  repeats  the Chime Tone & Voice.;Verify'
#string = 'Verify the Warning Tone Generator stops producing the Voice.;Verify'
#string = 'Verify the Warning Tone Generator produces a Chime Tone & Voice "FIRE ENGINE ONE FIRE".'
#string = '28. Verify the Warning Tone Generator produces a Chime Tone & Voice "FIRE ENGINE TWO FIRE".; Verify'
#string = 'Verify the Warning Tone Generator produces a Modulated Tone.'
string = 'Verify the Warning Tone Generator is NOT producing the Chime Tone.'

match1 = re.search(r'Warning Tone Generator', string)
print(match1)
str_temp = string[match1.end():]
match2 = re.search(r'produces a Chime Tone & Voice .*', str_temp)
print(match2)
match5 = re.search(r'(\sNOT\s|\sstops\s)', str_temp)
print(match5)
match6 = re.search(r'\sproduc.*\sTone', str_temp)
print(match6)
if match5 is not None:
    tone_voice = 'None'
elif match2 is not None:
    match3 = re.search(r'\".*\"', match2.group())
    tone_voice = match3.group()
elif match6 is not None:
    tone_voice = match6.group().split()[-2] + ' Tone'

match4 = re.search(r' repeats ', str_temp)
print(match4)
if match4 is not None:
    tone_voice = 'repeat last voice'

print('****************************')
print(tone_voice)
'''

