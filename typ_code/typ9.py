# analyseTyp7 gibt einen Wert zurück der so aussieht: action, breaker, location
# On the SIL, pull the 2DCU 26 VAC breaker out on the bottom row of the SIL Rack.
# action: pull
# breaker: ['2DCU 26 VAC']
# location: bottom row of the SIL Rack

import re


# function that contains the logic for matching the instruction, as well as extracting data fields
def typ_breaker(string):
    match1 = re.search(r'(?<=,\s)\w*\s*the\s*.*(?=\s+(b|B)reaker)', string)
    match2 = re.search(r'\s*the\s*', match1.group())
    match3 = re.search(r'(?<=on the\s).*', string)

    action = match1.group()[: match2.start()]
    breaker = match1.group()[match2.end():].strip(' ')
    if '&' in breaker:
        breaker = [w for w in breaker.replace('&', '').split() if w != ' ']
    else:
        breaker = [breaker]
    location = match3.group().strip('.')
    return action, breaker, location


# implementation of analyse(string)
# returns 1 if string matches type 3, else returns 0
def analyse(string):
    try:
        typ_breaker(string)
        return 1
    except AttributeError:
        return 0


# implementation of extract(string)
# returns dictionary with elements of instruction of type 3
def extract(string):
    action, breaker, location = typ_breaker(string)
    dic = {
        "Action": action.capitalize(),
        "Breaker": breaker,
        "Location": location,
    }
    return dic

'''
#string = 'On the SIL, pull the 2DCU 26 VAC breaker out on the bottom row of the SIL Rack.'
string = 'On the SIL, push the 1IPC & 2IPC breakers in on the SIL Rack.'
match1 = re.search(r'(?<=,\s)\w*\s*the\s*.*(?=\s+(b|B)reaker)', string)
print(match1)
match2 = re.search(r'\s*the\s*', match1.group())
print(match2)
match3 = re.search(r'(?<=on the\s).*', string)
print(match3)
action = match1.group()[: match2.start()]
breaker = match1.group()[match2.end():]
if '&' in breaker:
    breaker = [w for w in breaker.replace('&', '').split() if w != ' ']
else:
    breaker = [breaker]
location = match3.group().strip('.')
print(action)
print(breaker)
print(location)
'''
