# create a new .csv file to store unbekannte typ
# Output: Die Zahl der Anweisungen für jeden Typ
# Every time you run this program, you need to colse the saved unbekannt_typ.csv file.
from typ_code import typ1, typ3, typ4, typ5, typ6, typ7, typ8, typ9, typ10, typ11
import os
import pandas as pd
import numpy as np

folder = "source"
for k in range(len(os.listdir(folder))):
    filepath = folder + "/" + os.listdir(folder)[k]
    filename, file_extension = os.path.splitext(os.listdir(folder)[k])
    print("Lade: " + filename)

    if file_extension == ".csv":
        # read csv file
        file = pd.read_csv(filepath)
    elif file_extension == ".xlsx":
        file = pd.read_excel(filepath)           # note: len(file)+1=data.nrows, because the data obtained with the
                                                 # padas package will automatically delete the first row of headers,
    nrows = len(file)                            # and the second row of data will be index 0.


    print("Geladen: " + filepath)
    file = file.to_numpy()

    count = 0
    j = 0
    note = 0
    t1 = 0
    t3 = 0
    t4 = 0
    t5 = 0
    t6 = 0
    t7 = 0
    t8 = 0
    t9 = 0
    t10 = 0
    t11 = 0

    for i in range(len(file)):
        row = file[i]
        zeile = i + 2
        if row[0].startswith('Note') or row[0].startswith('NOTE'):  # check if row contains note or instruction
            note += 1

        elif typ1.analyse(row[0]):
            #print('tpy1:', i+1)
            t1 += 1

        elif typ3.analyse(row[0]):
            #print('tpy3:', i+1)
            t3 += 1

        elif typ4.analyse(row[0]):
            #print('typ4:', i+1)
            t4 += 1

        elif typ5.analyse(row[0]):
            #print('typ5:', i+1)
            t5 += 1

        elif typ6.analyse(row[0]):
            #print('tpy_6:', i+1)
            t6 += 1

        elif typ7.analyse(row[0]):
            #print('typ7:', i + 1)
            t7 += 1

        elif typ8.analyse(row[0]):
            #print('typ8:', i + 1)
            t8 += 1

        elif typ9.analyse(row[0]):
            #print('typ9:', i + 1)
            t9 += 1

        elif typ10.analyse(row[0]):
            # print('Typ "CDU_verify_read": ', zeile)
            t10 += 1

        elif typ11.analyse(row[0]):
            t11 += 1
            j = i + 1
            while not file[j][0].startswith('On CDU'):
                j += 1
                if j == nrows - 1:
                    break

        elif i < j:
            pass

        else:
            count += 1
            row_new = np.hstack((row, zeile))
            #row.append(str(zeile))
            if count == 1:
                data = row_new
            else:
                data = np.vstack((data, row_new))
            #print("unbekannte Typ :", i+1)

    frame = pd.DataFrame(data)
    frame.to_excel("Unbekannt" + filename + ".xlsx")

    print('unbekannt_num:', count)
    print('note_num:', note)
    print('t1_num:', t1)
    print('t3_num:', t3)
    print('t4_num:', t4)
    print('t5_num:', t5)
    print('t6_num:', t6)
    print('t7_num:', t7)
    print('t8_num:', t8)
    print('t9_num:', t9)
    print('t10_num:', t10)
    print('t11_num:', t11)
    print('***********************************')

