import xml.etree.ElementTree as ET
import os
import pandas as pd
import utils as utils
from verify_utils import NotImplementedException, UserActionException, VerifyException, NoteException
import variables_tables as vt

# Set Source folder
folder = "0_source"
files = os.listdir(folder)
# Comment/Uncomment this to loop through all files inside source.
#files = [files[0]]

# For-loop iterates through all files in the source folder.
for file in files:

    # Identify the file type and load the file into the workspace in numpy format.
    file_path = folder + "/" + file
    file_name, file_extension = os.path.splitext(file)
    if file_extension == ".csv":
        data = pd.read_csv(file_path)
    elif file_extension == ".xlsx":
        data = pd.read_excel(file_path)
    print("Loaded: " + file_path)
    data = data.to_numpy()

    # Create tree and root of XML-file
    root = ET.Element(file_name + 'XML')
    tree = ET.ElementTree(root)

    # Set helper variables
    line = 2  # Panda recognizes the first row as a header and deletes it
    nInstructions = len(data)
    counterNotes = 0
    counterUserActions = 0
    counterNodeSuccess = 0
    counterNodeFail = 0

    # Iterate through the rows of the panda.DataFrame and perform operations on each row
    for row in data:
        # Analyse the data entry and recognize sentence (line) type
        line_type = utils.analyse(row)

        # Extract the data fields from the data entry and store extracted fields in a dictionary
        info = utils.extract(row, line_type)

        # Create an XML node. Its subElements are the instruction's actions and variables (contained in info)
        xml_node_current = utils.createXMLnode(line_type, line, row, info)

        # Call function to verify action fields of node. If verification unsuccessful, function raises Exception.
        try:
            utils.verify_actions(xml_node_current, line_type)
            counterNodeSuccess += 1

        except VerifyException as e:
            #if line not in utils.known_verify_bugs:
            #print("############################# Reported Exception: " + str(e) + "########## See below:")
            #print(utils.createNodeReport(xml_node_current, info, row))
            counterNodeFail += 1
            xml_node_current = utils.createXMLnode("no_type", line, row, info)
            # Code continues after Verification section

        except NotImplementedException as e:
            #print("############################# Reported Exception: " + str(e) + "########## See below:")
            #print(utils.createNodeReport(xml_node_current, info, row))
            xml_node_current = utils.createXMLnode("no_type", line, row, info)
            # Code continues after Verification section

        except NoteException:
            counterNotes += 1
            pass

        except UserActionException:
            counterUserActions += 1

        # Connect node's sub_elements/actions to tree (As comments: debugging comments, notes - As elements: user_actions, machine_instructions)
        root.extend(xml_node_current)
        line += 1

    # Save XML tree as file
    destination = "0_destination/"
    tree.write(destination + file_name + '.xml', encoding='utf-8', xml_declaration=True)

    # Print the final report to console
    print("################# FINAL REPORT#################")
    print("In file \"" + file_name + "\" there are " + str(nInstructions) + " Lines.")
    print("There are " + str(counterNodeSuccess) + " lines that were identified, turned into automatic actions and all its variables have been verified (%s of all lines inside document)." % '{:.1%}'.format(counterNodeSuccess / nInstructions))
    print("There are " + str(counterNodeFail) + " lines that were identified, turned into automatic actions but failed verification. These have been amended as user_actions.")
    print("There are " + str(counterUserActions) + " lines were identified as user_actions and amended into XML structur as user_actions.")
    print("There are " + str(counterNotes) + " notes. These have been deleted.")
    print("%s of all lines inside document were inserted into XML." % '{:.1%}'.format((counterNodeSuccess+counterNodeFail+counterNotes+counterUserActions) / nInstructions))

######################################################## This part below wont be reached. Delete after typ11 fixed ###########

    # Deprecated code
    continue
    # delete?: for individual test, bei 'None' bitte entsprechende Infos einfügen.z.B:task1.analyse, Info['Device']
    j = 0
    text = []

    if typ11.analyse(instruction):
        j = i + 1
        while not data[j][0].startswith('On CDU'):
            text.append(data[j][0])
            j += 1
            if j == nInstructions - 1:
                break

        #print('Typ "waypoints_text": ', line)
        info = typ11.extract(instruction)
        line = ET.SubElement(root, 'line')
        line.text = str(line)
        XML.xml_waypoints_text(root, info['Device'], info['Location'], text)
        text = []

    elif i < j:
        pass